class Club < ActiveRecord::Base
  has_many :bookings
  has_many :bands, through: :bookings
  accepts_nested_attributes_for :bands
  accepts_nested_attributes_for :bookings

end


