class Band < ActiveRecord::Base
  has_many :bookings
  has_many :clubs, through: :bookings
  accepts_nested_attributes_for :bookings
  accepts_nested_attributes_for :clubs
end
