class Booking < ActiveRecord::Base
  belongs_to :band
  belongs_to :club
  accepts_nested_attributes_for :club
  accepts_nested_attributes_for :band
end